var data = {
    message: "Welcome to dogomatic! This is the amazing kennel that has all your favourite little fluffy doggos! Currently we have the following love-lumps.",
    dogs: []
}

Vue.component('dog-table', {
    template:`
      <div>
        <md-table v-model="dogs" md-sort="name" md-sort-order="asc" md-card md-fixed-header @md-selected="onDogSelect">
          <md-table-toolbar>
            <h1 class="md-title">Dogs</h1>
            <md-button class="md-icon-button" :disabled="!buttonsEnabled" @click="onPetDog">
              <md-icon>pets</md-icon>
            </md-button>

            <md-button class="md-icon-button" :disabled="!buttonsEnabled" @click="onBreedDog">
              <md-icon>child_care</md-icon>
            </md-button>
          </md-table-toolbar>

          <md-table-row slot="md-table-row" slot-scope="{ item }" md-selectable="single">
            <md-table-cell md-label="ID" md-sort-by="id" md-numeric>{{ item.id }}</md-table-cell>
            <md-table-cell md-label="Name" md-sort-by="name">{{ item.name }}</md-table-cell>
            <md-table-cell md-label="Breed" md-sort-by="breed" md-numeric>{{ item.breed }}</md-table-cell>
            <md-table-cell md-label="Age" md-sort-by="age" md-numeric>{{ item.age }}</md-table-cell>
            <md-table-cell md-label="Pet count" md-sort-by="petted_count" md-numeric>{{ item.petted_count }}</md-table-cell>
          </md-table-row>
        </md-table>
    </div>
    `,
    data: function() {
        return {
            selectedDog: null
        }
    },
    props: ['dogs'],
    methods: {
        onDogSelect: function(selected) {
            this.selectedDog = selected
        },
        onPetDog: function() {
            var dog = this.selectedDog
            var id = dog.id
            axios.get('http://31.208.209.168/api/dogs/pet/' + id)
            .then(response => {
                dog.petted_count = response.data.new_pet_count
            })
            .catch(error => {
                console.log(error)
            })
        },
        onBreedDog: function() {
            this.$router.push('breed/' + this.selectedDog.id)
        }
    },
    computed: {
        buttonsEnabled() {
            return this.selectedDog != null
        }
    }
})

const dogIndex = Vue.extend({
    data: function() {
        return data
    },
    template:`
        <div>
            <p>{{ message }}</p>
            <dog-table v-bind:dogs="dogs"/>
        </div>
    `,
    created() {
        axios.get('http://31.208.209.168/api/dogs')
        .then(response => {
             this.dogs = response.data.dogs
        })
        .catch(error => {
          console.log(error);
        })
    }
})

const dogBreed = Vue.extend({
    data: function() {
        return  {
            dogs: data.dogs,
            breed_dad: null,
            breed_mom: null,
            mom_options: [],
            puppy_name: ""
        }
    },
    template:`
        <div>
            <p>Oh, so you wanna breed a dog! Exciting! Your selected dog dad is {{ dogNameFromId(breed_dad) }} so just go on and select a fitting mother below! :) Woof!!</p>
            <p>Dad: {{ dogNameFromId(breed_dad) }}</p>
            <md-field>
              <label for="mom_option">Mom</label>
              <md-select v-model="breed_mom" name="mom_option" id="mom_option">
                <md-option v-for="dog in dogs" :value="parseInt(dog.id)">{{dog.name}}</md-option>
              </md-select>
            </md-field>
            <md-field>
                <label>Puppy name</label>
                <md-input v-model="puppy_name"></md-input>
            </md-field>
            <md-button class="md-raised md-primary" :disabled="!canBreed" @click="breed">Breed</md-button>
        </div>
    `,
    computed: {
        canBreed() {
            return this.breed_mom != null && this.puppy_name != ""
        }
    },
    created() {
        this.breed_dad = parseInt(this.$route.params.id)
        this.breed_mom = null

        //quite hacky but oh well
        if(this.dogs.length == 0)
        {
            axios.get('http://31.208.209.168/api/dogs')
            .then(response => {
                 this.dogs = response.data.dogs
            })
            .catch(error => {
              console.log(error);
            })
        }
    },
    watch: {
        '$route' (to, from) {
            this.breed_dad = parseInt(this.$route.params.id)
            this.breed_mom = null
        }
    },
    methods: {
        dogNameFromId: function(id) {
            if(this.dogs.length > 0)
                return this.dogs.find(x => x.id === id).name
            else
                return "..."
        },
        breed: function() {
            console.log("gonna breed puppy " + this.puppy_name + " with dad " + this.breed_dad + " and mom " + this.breed_mom)
            axios.get('http://31.208.209.168/api/dogs/breed/' + this.breed_dad + '/' + this.breed_mom + '/' + this.puppy_name)
            .then(response => {
                this.$router.push('/dogs')
            })
            .catch(error => {
              console.log(error);
            })
        }
    }
})

const Foo = { template: '<p>asdf</p>'}

const routes = [
    { path: '/dogs', component: dogIndex},
    { path: '/breed/:id', component: dogBreed}
]

const router = new VueRouter({
    routes: routes
})

var vm = new Vue({
    router: router
}).$mount('#app')
